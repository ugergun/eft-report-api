## Overview

Eft Reporting Api developed for EFT Software.

This project is a kind of middleware built on Clearsettle Reporting API.

Eft Reporting Api provide all of Clearsettle Reporting Api functions of endpoints.

```
Clearsettle Endpoints                             Eft Endpoints

/api/v3/merchant/user/login                       /merchant/user/login

/api/v3/transactions/report                       /transactions/report

/api/v3/transaction/list                          /transaction/list

/api/v3/transaction                               /transaction

/api/v3/client                                    /client

/api/v3/merchant                                  /merchant
```

You can access Clearsette Reporting Api Documentation : [Clearsettle](https://bitbucket.org/ugergun/eft-report-api/src/97fe79b5f4344c13c028216d45c185003ff1a5a6/ReportingAPIDocumentation.pdf?at=master&fileviewer=file-view-default)


Also this project provide another endpoint differently in others. You can check total amount of all of ipn transactions by currency between dates.

```
Total Amount End Point

url : /transaction/totalAmount
request param:
             {
                fromDate : date(YYYY-MM-DD)
                toDate   : date(YYYY-MM-DD)
                currency : string
             }

response : totalAmount type : int
```

##Considerations

When you want to access any endpoint , you have to know request type. The project based on json to object mechanism. So you must put "content-type : application-json" header of request.
Also know sending data must be json.


