package com.clearsettle.reporting.controller;

import com.clearsettle.reporting.api.ReportingApiClient;
import com.clearsettle.reporting.dto.RestResponse;
import com.clearsettle.reporting.dto.StatusEnum;
import com.clearsettle.reporting.dto.special.IpnTotalAmountRequest;
import com.clearsettle.reporting.dto.transaction.TransactionQueryRequest;
import com.clearsettle.reporting.dto.transaction.TransactionRequest;
import com.clearsettle.reporting.dto.transaction.report.TransactionReportRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TransactionController {


    @Autowired
    transient ReportingApiClient apiClient;

    @Autowired
    transient MessageSource messageSource;

    static Logger logger = LoggerFactory.getLogger(TransactionController.class);

    @RequestMapping(value = "/transactions/report", method = RequestMethod.POST)
    public RestResponse getTransactionReport(@RequestBody TransactionReportRequest request) {
        try {
            return apiClient.getTransactionReport(request);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new RestResponse(806, StatusEnum.ERROR, messageSource.getMessage("unexpected.error", null, LocaleContextHolder.getLocale()));
        }
    }


    @RequestMapping(value = "/transaction/list", method = RequestMethod.POST)
    public RestResponse getTransactionQuery(@RequestBody TransactionQueryRequest request) {
        try {
            return apiClient.getTransactionQuery(request);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new RestResponse(806, StatusEnum.ERROR, messageSource.getMessage("unexpected.error", null, LocaleContextHolder.getLocale()));
        }
    }


    @RequestMapping(value = "/transaction", method = RequestMethod.POST)
    public RestResponse getTransaction(@RequestBody TransactionRequest request) {
        try {
            return apiClient.getTransaction(request);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new RestResponse(806, StatusEnum.ERROR, messageSource.getMessage("unexpected.error", null, LocaleContextHolder.getLocale()));
        }
    }


    @RequestMapping(value = "/transaction/totalAmount", method = RequestMethod.POST)
    public RestResponse getTotalAmount(@RequestBody IpnTotalAmountRequest request) {
        try {
            return apiClient.getIpnTotalAmount(request);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new RestResponse(806, StatusEnum.ERROR, messageSource.getMessage("unexpected.error", null, LocaleContextHolder.getLocale()));
        }
    }

}
