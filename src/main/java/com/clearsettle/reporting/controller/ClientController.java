package com.clearsettle.reporting.controller;


import com.clearsettle.reporting.api.ReportingApiClient;
import com.clearsettle.reporting.dto.RestResponse;
import com.clearsettle.reporting.dto.StatusEnum;
import com.clearsettle.reporting.dto.client.ClientRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClientController {


    @Autowired
    transient ReportingApiClient apiClient;

    @Autowired
    transient MessageSource messageSource;

    private static Logger logger = LoggerFactory.getLogger(TransactionController.class);

    @RequestMapping(value = "/client", method = RequestMethod.POST)
    public RestResponse getClient(@RequestBody ClientRequest request) {
        try {
            return apiClient.getClient(request);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new RestResponse(806, StatusEnum.ERROR, messageSource.getMessage("unexpected.error", null, LocaleContextHolder.getLocale()));
        }
    }

}
