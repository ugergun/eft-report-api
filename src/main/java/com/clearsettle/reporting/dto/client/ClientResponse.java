package com.clearsettle.reporting.dto.client;

import com.clearsettle.reporting.dto.RestResponse;
import com.clearsettle.reporting.dto.data.CustomerInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientResponse extends RestResponse {

    private CustomerInfo customerInfo;

    public CustomerInfo getCustomerInfo() {
        return customerInfo;
    }

    public void setCustomerInfo(CustomerInfo customerInfo) {
        this.customerInfo = customerInfo;
    }
}
