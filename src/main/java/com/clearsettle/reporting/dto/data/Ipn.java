package com.clearsettle.reporting.dto.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Ipn {

    private Boolean sent;

    private IpnMerchant merchant;

    public Boolean getSent() {
        return sent;
    }

    public void setSent(Boolean sent) {
        this.sent = sent;
    }

    public IpnMerchant getMerchant() {
        return merchant;
    }

    public void setMerchant(IpnMerchant merchant) {
        this.merchant = merchant;
    }
}
