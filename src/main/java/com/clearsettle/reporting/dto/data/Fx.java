package com.clearsettle.reporting.dto.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Fx {

    private FxMerchant merchant;

    public FxMerchant getMerchant() {
        return merchant;
    }

    public void setMerchant(FxMerchant merchant) {
        this.merchant = merchant;
    }
}
