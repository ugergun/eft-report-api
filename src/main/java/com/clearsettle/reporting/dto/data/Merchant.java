package com.clearsettle.reporting.dto.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Merchant {

    private String ipnUrl;

    private Integer id;

    private String parentId;

    private String cpgKey;

    private String comType;

    private String secretKey;

    private String descriptor;

    private String mcc;

    private String name;

    private String type;

    private Boolean allowPartialRefund;

    private Boolean allowPartialCapture;

    private String threeGStatus;

    private String apiKey;

    public String getIpnUrl() {
        return ipnUrl;
    }

    public void setIpnUrl(String ipnUrl) {
        this.ipnUrl = ipnUrl;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getCpgKey() {
        return cpgKey;
    }

    public void setCpgKey(String cpgKey) {
        this.cpgKey = cpgKey;
    }

    public String getComType() {
        return comType;
    }

    public void setComType(String comType) {
        this.comType = comType;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getDescriptor() {
        return descriptor;
    }

    public void setDescriptor(String descriptor) {
        this.descriptor = descriptor;
    }

    public String getMcc() {
        return mcc;
    }

    public void setMcc(String mcc) {
        this.mcc = mcc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty(value = "3dStatus")
    public String getThreeGStatus() {
        return threeGStatus;
    }

    public void setThreeGStatus(String threeGStatus) {
        this.threeGStatus = threeGStatus;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public Boolean getAllowPartialRefund() {
        return allowPartialRefund;
    }

    public void setAllowPartialRefund(Boolean allowPartialRefund) {
        this.allowPartialRefund = allowPartialRefund;
    }

    public Boolean getAllowPartialCapture() {
        return allowPartialCapture;
    }

    public void setAllowPartialCapture(Boolean allowPartialCapture) {
        this.allowPartialCapture = allowPartialCapture;
    }
}
