package com.clearsettle.reporting.dto.data;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Transaction {

    private TransactionMerchant merchant;

    public TransactionMerchant getMerchant() {
        return merchant;
    }

    public void setMerchant(TransactionMerchant merchant) {
        this.merchant = merchant;
    }
}
