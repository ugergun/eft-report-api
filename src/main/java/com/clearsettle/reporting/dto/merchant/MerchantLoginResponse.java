package com.clearsettle.reporting.dto.merchant;

import com.clearsettle.reporting.dto.RestResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MerchantLoginResponse extends RestResponse {

    private String token;

    public MerchantLoginResponse() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
