package com.clearsettle.reporting.dto.merchant;

import com.clearsettle.reporting.dto.RestResponse;
import com.clearsettle.reporting.dto.data.Merchant;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MerchantResponse extends RestResponse {

    private Merchant merchant;

    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }
}
