package com.clearsettle.reporting.dto.transaction;

import com.clearsettle.reporting.dto.RestResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionQueryResponse extends RestResponse {


    private Integer perPage;


    private Integer currentPage;

    private String nextPageUrl;

    private String prevPageUrl;

    private Integer from;

    private Integer to;

    private List<TransactionResponse> data;

    @JsonProperty(value = "per_page")
    public Integer getPerPage() {
        return perPage;
    }

    public void setPerPage(Integer perPage) {
        this.perPage = perPage;
    }

    @JsonProperty(value = "current_page")
    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    @JsonProperty(value = "next_page_url")
    public String getNextPageUrl() {
        return nextPageUrl;
    }

    public void setNextPageUrl(String nextPageUrl) {
        this.nextPageUrl = nextPageUrl;
    }

    @JsonProperty(value = "prev_page_url")
    public String getPrevPageUrl() {
        return prevPageUrl;
    }

    public void setPrevPageUrl(String prevPageUrl) {
        this.prevPageUrl = prevPageUrl;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getTo() {
        return to;
    }

    public void setTo(Integer to) {
        this.to = to;
    }

    public List<TransactionResponse> getData() {
        return data;
    }

    public void setData(List<TransactionResponse> data) {
        this.data = data;
    }
}
