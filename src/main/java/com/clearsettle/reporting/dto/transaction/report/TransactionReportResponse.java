package com.clearsettle.reporting.dto.transaction.report;

import com.clearsettle.reporting.dto.RestResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionReportResponse extends RestResponse {

    private List<ReportResponse> response;

    public List<ReportResponse> getResponse() {
        return response;
    }

    public void setResponse(List<ReportResponse> response) {
        this.response = response;
    }
}
