package com.clearsettle.reporting.dto;

public class RestResponse {

    private Integer code;

    private StatusEnum status;

    private String message;

    public RestResponse(Integer code, StatusEnum status, String message) {
        this.code = code;
        this.status = status;
        this.message = message;
    }

    public RestResponse() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
