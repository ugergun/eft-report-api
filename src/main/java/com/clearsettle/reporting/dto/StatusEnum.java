package com.clearsettle.reporting.dto;

public enum StatusEnum {

    APPROVED,
    WAITING,
    DECLINED,
    ERROR
}
