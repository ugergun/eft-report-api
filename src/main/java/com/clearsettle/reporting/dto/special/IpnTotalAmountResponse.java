package com.clearsettle.reporting.dto.special;


import com.clearsettle.reporting.dto.RestResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class IpnTotalAmountResponse extends RestResponse {

    private Integer totalAmount;

    public IpnTotalAmountResponse(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public IpnTotalAmountResponse() {
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }
}
