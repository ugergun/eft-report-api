package com.clearsettle.reporting.utility;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Service
public class ApiConfig extends Properties {

    public static Logger logger = LoggerFactory.getLogger(ApiConfig.class);

    @PostConstruct
    private void init() {
        try {
            InputStream input = new FileInputStream("src/main/resources/api-config.properties");
            load(input);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
    }

}
