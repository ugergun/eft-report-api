package com.clearsettle.reporting.api;

import com.clearsettle.reporting.dto.RestResponse;
import com.clearsettle.reporting.dto.client.ClientRequest;
import com.clearsettle.reporting.dto.client.ClientResponse;
import com.clearsettle.reporting.dto.merchant.MerchantLoginRequest;
import com.clearsettle.reporting.dto.merchant.MerchantLoginResponse;
import com.clearsettle.reporting.dto.merchant.MerchantRequest;
import com.clearsettle.reporting.dto.merchant.MerchantResponse;
import com.clearsettle.reporting.dto.special.IpnTotalAmountRequest;
import com.clearsettle.reporting.dto.special.IpnTotalAmountResponse;
import com.clearsettle.reporting.dto.transaction.TransactionQueryRequest;
import com.clearsettle.reporting.dto.transaction.TransactionQueryResponse;
import com.clearsettle.reporting.dto.transaction.TransactionRequest;
import com.clearsettle.reporting.dto.transaction.TransactionResponse;
import com.clearsettle.reporting.dto.transaction.report.TransactionReportRequest;
import com.clearsettle.reporting.dto.transaction.report.TransactionReportResponse;
import com.clearsettle.reporting.session.Authentication;
import com.clearsettle.reporting.utility.ApiConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class ReportingApiClient implements ReportingApi {

    @Autowired
    transient ApiConfig apiConfig;

    @Autowired
    transient RestTemplateFactory restTemplateFactory;

    @Autowired
    transient Authentication authentication;

    public MerchantLoginResponse merchantLogin(MerchantLoginRequest request) {
        String url = apiConfig.getProperty("merchant.login");
        RestTemplate template = restTemplateFactory.newInstance();
        MerchantLoginResponse response = template.postForObject(url, request, MerchantLoginResponse.class);
        if (response != null) {
            authentication.setEmail(request.getEmail());
            authentication.setToken(response.getToken());
        }
        return response;
    }

    public TransactionReportResponse getTransactionReport(TransactionReportRequest request) {
        String url = apiConfig.getProperty("transaction.report");
        RestTemplate template = restTemplateFactory.newInstance();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", authentication.getToken());

        HttpEntity<TransactionReportRequest> entity = new HttpEntity<>(request, headers);
        TransactionReportResponse response = template.postForObject(url, entity, TransactionReportResponse.class);
        checkToken(response);
        return response;
    }


    public TransactionQueryResponse getTransactionQuery(TransactionQueryRequest request) {
        String url = apiConfig.getProperty("transaction.query");
        RestTemplate template = restTemplateFactory.newInstance();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", authentication.getToken());

        HttpEntity<TransactionQueryRequest> entity = new HttpEntity<>(request, headers);
        TransactionQueryResponse response = template.postForObject(url, entity, TransactionQueryResponse.class);
        checkToken(response);
        return response;
    }


    public TransactionResponse getTransaction(TransactionRequest request) {
        String url = apiConfig.getProperty("transaction");
        RestTemplate template = restTemplateFactory.newInstance();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", authentication.getToken());

        HttpEntity<TransactionRequest> entity = new HttpEntity<>(request, headers);
        TransactionResponse response = template.postForObject(url, entity, TransactionResponse.class);
        checkToken(response);
        return response;
    }


    public ClientResponse getClient(ClientRequest request) {
        String url = apiConfig.getProperty("client");
        RestTemplate template = restTemplateFactory.newInstance();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", authentication.getToken());

        HttpEntity<ClientRequest> entity = new HttpEntity<>(request, headers);
        ClientResponse response = template.postForObject(url, entity, ClientResponse.class);
        checkToken(response);
        return response;
    }

    public MerchantResponse getMerchant(MerchantRequest request) {
        String url = apiConfig.getProperty("merchant");
        RestTemplate template = restTemplateFactory.newInstance();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", authentication.getToken());

        HttpEntity<MerchantRequest> entity = new HttpEntity<>(request, headers);
        MerchantResponse response = template.postForObject(url, entity, MerchantResponse.class);
        checkToken(response);
        return response;
    }

    public IpnTotalAmountResponse getIpnTotalAmount(IpnTotalAmountRequest request) {
        TransactionQueryRequest transactionQueryRequest = new TransactionQueryRequest();
        transactionQueryRequest.setFromDate(request.getFromDate());
        transactionQueryRequest.setToDate(request.getToDate());
        TransactionQueryResponse response = getTransactionQuery(transactionQueryRequest);
        List<TransactionResponse> data = response.getData();
        return new IpnTotalAmountResponse(data.stream().filter(item -> item.getIpn() != null && item.getIpn().getMerchant().getCurrency().equals(request.getCurrency())).mapToInt(item -> item.getIpn().getMerchant().getAmount()).sum());
    }


    /**
     * if token has expired, authentication token is removed automatically
     *
     * @param response
     */
    private void checkToken(RestResponse response) {
        if (response.getMessage() != null && response.getMessage().equals(apiConfig.getProperty("token.expired"))) {
            authentication.setToken(null);
        }
    }


}
