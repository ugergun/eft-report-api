package com.clearsettle.reporting.api;

import com.clearsettle.reporting.dto.client.ClientRequest;
import com.clearsettle.reporting.dto.client.ClientResponse;
import com.clearsettle.reporting.dto.merchant.MerchantLoginRequest;
import com.clearsettle.reporting.dto.merchant.MerchantLoginResponse;
import com.clearsettle.reporting.dto.merchant.MerchantRequest;
import com.clearsettle.reporting.dto.merchant.MerchantResponse;
import com.clearsettle.reporting.dto.transaction.TransactionQueryRequest;
import com.clearsettle.reporting.dto.transaction.TransactionQueryResponse;
import com.clearsettle.reporting.dto.transaction.TransactionRequest;
import com.clearsettle.reporting.dto.transaction.TransactionResponse;
import com.clearsettle.reporting.dto.transaction.report.TransactionReportRequest;
import com.clearsettle.reporting.dto.transaction.report.TransactionReportResponse;

public interface ReportingApi {

    public MerchantLoginResponse merchantLogin(MerchantLoginRequest request);

    public TransactionReportResponse getTransactionReport(TransactionReportRequest request);

    public TransactionQueryResponse getTransactionQuery(TransactionQueryRequest request);

    public TransactionResponse getTransaction(TransactionRequest request);

    public ClientResponse getClient(ClientRequest request);

    public MerchantResponse getMerchant(MerchantRequest request);
}
