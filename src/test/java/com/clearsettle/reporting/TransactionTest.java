package com.clearsettle.reporting;


import com.clearsettle.reporting.api.RestTemplateFactory;
import com.clearsettle.reporting.dto.merchant.MerchantLoginRequest;
import com.clearsettle.reporting.dto.merchant.MerchantLoginResponse;
import com.clearsettle.reporting.dto.transaction.TransactionQueryRequest;
import com.clearsettle.reporting.dto.transaction.TransactionQueryResponse;
import com.clearsettle.reporting.dto.transaction.TransactionRequest;
import com.clearsettle.reporting.dto.transaction.TransactionResponse;
import com.clearsettle.reporting.utility.ApiConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TransactionTest {


    @Autowired
    ApiConfig apiConfig;

    @Autowired
    RestTemplateFactory templateFactory;


    @Test
    public void getTransaction() throws Exception {

        RestTemplate template = templateFactory.newInstance();
        TransactionRequest request = new TransactionRequest();
        MerchantLoginResponse response = login(template);
        request.setTransactionId("841917-1482912015-3");
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        headers.set("Authorization", response.getToken());

        HttpEntity<TransactionRequest> entity = new HttpEntity<>(request, headers);
        TransactionResponse transactionResponse = template.postForObject(apiConfig.getProperty("transaction"), entity, TransactionResponse.class);
    }

    @Test
    public void getTransactionQuery() throws Exception {

        RestTemplate template = templateFactory.newInstance();
        TransactionQueryRequest request = new TransactionQueryRequest();
        MerchantLoginResponse response = login(template);
        request.setFromDate(Date.valueOf(LocalDate.now().minusDays(60)));
        request.setToDate(Date.valueOf(LocalDate.now()));
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        headers.set("Authorization", response.getToken());

        HttpEntity<TransactionQueryRequest> entity = new HttpEntity<>(request, headers);
        TransactionQueryResponse transactionQueryResponse = template.postForObject(apiConfig.getProperty("transaction.query"), entity, TransactionQueryResponse.class);
        List<TransactionResponse> data = transactionQueryResponse.getData();
        Integer totalEuroAmount = data.stream().filter(item -> item.getIpn() != null && item.getIpn().getMerchant().getCurrency().equals("EUR")).mapToInt(item -> item.getIpn().getMerchant().getAmount()).sum();
        System.out.println(totalEuroAmount);


    }


    private MerchantLoginResponse login(RestTemplate template) {

        MerchantLoginRequest merchantLoginRequest = new MerchantLoginRequest();
        merchantLoginRequest.setEmail("demo@bumin.com.tr");
        merchantLoginRequest.setPassword("cjaiU8CV");

        return template.postForObject(apiConfig.getProperty("merchant.login"), merchantLoginRequest, MerchantLoginResponse.class);
    }

}
